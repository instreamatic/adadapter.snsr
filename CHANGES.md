# Changes history #

## 5.1.2 - Beta ##
- adapter: Added sending statistic detecting wake-phrase
- adapter: Turn-on detect-phrase when duration > 35

## 5.1.0 - Beta ##
- sensory adapter: Phrase-spot from maven-repository
- adman adapter: Added IVoiceRecognition for recognition on device

## 5.0.0 - Beta ##
- First beta version
