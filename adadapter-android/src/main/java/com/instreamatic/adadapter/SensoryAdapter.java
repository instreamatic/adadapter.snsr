package com.instreamatic.adadapter;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.instreamatic.sensory.core.PhraseSpot;

import java.io.File;

public final class SensoryAdapter {
    final private static String TAG = SensoryAdapter.class.getSimpleName();

    private PhraseSpot.EventListener phraseSpotEventListener;
    Handler tHandler;

    private PhraseSpot mPhraseSpot;
    private String mPhraseFile;

    public SensoryAdapter(PhraseSpot.EventListener eventListener) {
        this.phraseSpotEventListener = eventListener;
        if(tHandler == null) {
            tHandler = new Handler(Looper.getMainLooper());
        }
    }

    public void init(String mPhraseFile) {
        if (mPhraseFile == null) {
            Log.d(TAG, "mPhraseFile is null");
            return;
        }
        this.mPhraseFile = mPhraseFile;
    }

    public boolean start(final double timeout) {
        if (this.mPhraseSpot != null) {
            Log.d(TAG, "PhraseSpot is not null");
            return false;
        }
        tHandler.post(new Runnable() {
            @Override
            public void run()    {
                startDetectPhrase(timeout);
            }
        });
        return true;
    }

    public void stop() {
        tHandler.post(new Runnable() {
            @Override
            public void run()    {
                stopDetectPhrase();
            }
        });
    }

    private void startDetectPhrase(double timeout) {
        Log.d(TAG, "Phrase from: '" + mPhraseFile + "' among other speech.");
        String triggerPath = new File("assets/models", mPhraseFile).toString();
        mPhraseSpot = new PhraseSpot(triggerPath, timeout);
        Bundle extra = new Bundle();
        extra.putBoolean(PhraseSpot.SNSR_MODE_PHRASE_SPOT, true);
        mPhraseSpot.setParameters(extra);
        mPhraseSpot.setEventListener(this.phraseSpotEventListener);
        mPhraseSpot.start();

    }

    private void stopDetectPhrase() {
        Log.d(TAG, "stop detect phrase");
        if (mPhraseSpot != null) {
            mPhraseSpot.setEventListener(null);
            mPhraseSpot.stop();
            mPhraseSpot = null;
        }
    }
}
