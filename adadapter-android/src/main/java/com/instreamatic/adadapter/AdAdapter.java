package com.instreamatic.adadapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.event.PlayerEvent;
import com.instreamatic.adman.event.RequestEvent;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.adman.voice.VoiceEvent;
import com.instreamatic.player.IAudioPlayer;
import com.instreamatic.sensory.core.PhraseSpot;
import com.instreamatic.vast.model.VASTAd;
import com.sensory.speech.snsr.Snsr;
import com.sensory.speech.snsr.SnsrRC;
import com.sensory.speech.snsr.SnsrSession;

final public class AdAdapter implements  IAdAdapter{
    final private static String TAG = "AdAdapter";

    private EventDispatcher dispatcher;
    private AdmanAdapter admanAdapter;
    private AdmanRequest admanRequestDefault = new AdmanRequest.Builder()
            .setSiteId(1249)
            .setRegion(Region.GLOBAL)
            .setType(Type.VOICE)
            .build();

    private boolean startPlaying = false;
    private AdAdapter.AdmanEventListener admanEventListener = new AdAdapter.AdmanEventListener();

    private String pathModelPhraseSpot;
    private PhraseSpotEventListener phraseSpotEventListener = new PhraseSpotEventListener();
    private SensoryAdapter sensoryAdapter;
    private AdAdapter.Callback callback;
    private Boolean useDetectPhrase = false;
    private Boolean isDetectPhrase = false;


    public AdAdapter(Context context) {

        this.dispatcher = new EventDispatcher();

        admanAdapter = new AdmanAdapter(context, admanRequestDefault);

        EventDispatcher dispatcher = admanAdapter.getDispatcher();
        dispatcher.addListener(AdmanEvent.TYPE, admanEventListener);
        dispatcher.addListener(VoiceEvent.TYPE, admanEventListener);
        dispatcher.addListener(RequestEvent.TYPE, admanEventListener);
        dispatcher.addListener(PlayerEvent.TYPE, admanEventListener);

        sensoryAdapter = new SensoryAdapter(phraseSpotEventListener);
        Snsr.init(context);  // Required for direct access to assets
    }

    public void setStateListener(AdAdapter.Callback callback) {
        this.callback = callback;
    }

    public AdmanRequest updateRequest(AdmanRequest.Builder params, boolean allField) {
        return admanAdapter.updateRequest(params, allField);
    }

    public void updateModelPhraseSpot(String path) {
        this.pathModelPhraseSpot = path;
        sensoryAdapter.init(pathModelPhraseSpot);
    }

    public void setVASTBytes(byte[] byteArray, boolean startPlaying) {
        admanAdapter.setVASTBytes(byteArray, startPlaying);
    }

    public void start() {
        Log.d(TAG, "start");
        this.load(true);
    }

    public void preload(){
        Log.d(TAG, "preload");
        this.load(false);
    }

    public void pause(){
        Log.d(TAG, "pause");
        admanAdapter.pause();
    }

    public void play() {
        Log.d(TAG, "play");
        if (admanAdapter.getCurrentAd() == null) {
            return;
        }
        if (admanAdapter.isActive()) {
            admanAdapter.play();
        }
    }

    public void skip() {
        Log.d(TAG, "skip");
        admanAdapter.skip();
    }

    public void reset() {
        Log.d(TAG, "reset");
        admanAdapter.reset();
    }

    public boolean isPlaying() {
        boolean playing = admanAdapter.isPlaying();
        return  playing;
    }

    public boolean isActive() {
        boolean active = admanAdapter.isActive();
        return  active;
    }

    public void sendCanShow() {
        admanAdapter.sendCanShow();
    }

    public Bundle getMetadata() {
        return admanAdapter.getMetadata();
    }

    public AdmanAdapter getAdmanAdapter() {
        return admanAdapter;
    }

    public EventDispatcher getDispatcher() {
        return dispatcher;
    }

    public void setParameters(Bundle parameters) {
        admanAdapter.setParameters(parameters);
    }

    public void addListener(AdAdapterEvent.Listener listener) {
        dispatcher.addListener(AdAdapterEvent.TYPE, listener);
    }

    public void removeListener(AdAdapterEvent.Listener listener) {
        dispatcher.removeListener(AdAdapterEvent.TYPE, listener);
    }

    private void load(boolean startPlaying) {
        if (this.isActive()) {
            return;
        }
        this.startPlaying = startPlaying;
        admanAdapter.load(startPlaying);
    }

    private void setAdmanParameters(boolean silence, boolean audio_focus) {
        Bundle extra = new Bundle();
        extra.putBoolean("adman.need_silence_player", silence);
        setParameters(extra);
        AdmanVoice admanVoice = admanAdapter.adman != null ? admanAdapter.adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class) : null;
            if (admanVoice != null) {
                admanVoice.setAutoRecognition(!silence);
            }
    }

    public interface Callback {
        void onChangeDetectPhrase(AdAdapter sender, Boolean state);
        void onChangeRecognition(AdAdapter sender, Boolean state);
    }

    private class AdmanEventListener implements AdmanEvent.Listener, VoiceEvent.Listener, RequestEvent.Listener, PlayerEvent.Listener  {
        @Override
        public void onAdmanEvent(AdmanEvent event) {
            AdmanEvent.Type type = event.getType();
            Log.d(TAG, "onAdmanEvent: " + type);
            switch (type) {
                case PREPARE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PREPARE, TAG));
                    break;
                case NONE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.NONE, TAG));
                    break;
                case FAILED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.FAILED, TAG));
                    break;
                case READY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.READY, TAG));
                    break;
                case STARTED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STARTED, TAG));
                    break;
                case ALMOST_COMPLETE:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.ALMOST_COMPLETE, TAG));
                    break;
                case COMPLETED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.COMPLETED, TAG));
                    break;
                case SKIPPED:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.SKIPPED, TAG));
                    break;
            }
        }

        @Override
        public void onRequestEvent(RequestEvent event) {
            RequestEvent.Type type = event.getType();
            Log.d(TAG, "onRequestEvent: " + type);
            switch (type) {
                case SUCCESS:
                    VASTAd ad =  (event.ads != null && (event.ads.size() > 0)) ? event.ads.get(0) : null;
                    String typeVAST = ad != null ? ad.type : null;
                    useDetectPhrase = (typeVAST != null && typeVAST.toLowerCase().equals("vor"));
                    setAdmanParameters(useDetectPhrase, !useDetectPhrase);
                    break;
            }
        }

        @Override
        public void onVoiceEvent(VoiceEvent event) {
            VoiceEvent.Type type = event.getType();
            Log.d(TAG, "onVoiceEvent: " + type);
            switch (type) {
                case START:
                    if (AdAdapter.this.callback != null ) AdAdapter.this.callback.onChangeRecognition(AdAdapter.this, true);
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.START_RECOGNITION, TAG));
                    break;
                case STOP:
                    if (AdAdapter.this.callback != null ) AdAdapter.this.callback.onChangeRecognition(AdAdapter.this, false);
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STOP_RECOGNITION, TAG));
                    break;
            }
        }

        @Override
        public void onPlayerEvent(PlayerEvent event) {
            PlayerEvent.Type type = event.getType();
            switch (type) {
                case PLAY:
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PLAY, TAG));
                    break;
                case PROGRESS:
                    if (useDetectPhrase) {
                        IAudioPlayer player = admanAdapter.getМediaPlayer();
                        int duration = player == null ? 0 : player.getDuration();
                        int position = player == null ? 0 : player.getPosition();
                        double factor = duration > 0 ? (double)position / duration : 0.;
                        if (factor > 0.35) {
                            Log.d(TAG, "PROGRESS: " + position + " - " + duration + " " + factor);
                            isDetectPhrase = false;
                            if (AdAdapter.this.callback != null ) AdAdapter.this.callback.onChangeDetectPhrase(AdAdapter.this, true);
                            double timeout = duration - position;
                            boolean result = sensoryAdapter.start(timeout);
                            if (result) {
                                dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.START_DETECT, TAG));
                            }
                        }
                    }
                    dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PROGRESS, TAG));
                    break;
                case COMPLETE:
                    if (useDetectPhrase) {
                        sensoryAdapter.stop();
                        Log.d(TAG, "COMPLETE: useDetectPhrase " + useDetectPhrase + "; isDetectPhrase " + isDetectPhrase);
                        if (admanAdapter.adman != null) {
                            if (isDetectPhrase) {
                                AdmanVoice admanVoice = admanAdapter.adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
                                if (admanVoice != null) {
                                    admanVoice.startRecognition();
                                }
                            }
                            else {
                                AdmanVoice admanVoice = admanAdapter.adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
                                if (admanVoice != null) {
                                    admanVoice.complete();
                                } else {
                                    admanAdapter.skip();
                                }
                            }
                        }
                    }
                    break;
                case PAUSE:
                case FAILED:
                    if (useDetectPhrase) {
                        sensoryAdapter.stop();
                    }
                    if (type == PlayerEvent.Type.PAUSE) {
                        dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.PAUSE, TAG));
                    }
                    break;
            }
        }

    }

/*
    public SensoryAdapter getSensoryAdapter() {
        return sensoryAdapter;
    }

 */
    class PhraseSpotEventListener implements PhraseSpot.EventListener {
        public void onEvent(SnsrSession s, String key, SnsrRC res) {
            if (res == SnsrRC.STOP) {
                Log.d(TAG, "phrase: STOP or TIMED_OUT");
                dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STOP_DETECT, TAG));
                if (AdAdapter.this.callback != null ) AdAdapter.this.callback.onChangeDetectPhrase(AdAdapter.this, false);
            }
        }

        public void onNLUEvent(SnsrSession s, String s_name, String s_value) {
        }

        public void onResult(SnsrSession s, String text) {
            Log.d(TAG, "phrase: text");
            isDetectPhrase = true;
            dispatcher.dispatch(new AdAdapterEvent(AdAdapterEvent.Type.STOP_DETECT, TAG));
            admanAdapter.sendAction("embedded_wake_word");
            if (AdAdapter.this.callback != null ) AdAdapter.this.callback.onChangeDetectPhrase(AdAdapter.this, false);
            IAudioPlayer player = admanAdapter.getМediaPlayer();
            if (player != null) {
                player.complete();
            }
        }

        public void onError(SnsrSession s, String msg) {
        }

        public void onChangeProgress(SnsrSession s, double position, double duration) {
            //final double timeout = getDuration() - getPosition();
            //Log.d(TAG, "onChangeProgress, position: " + position + ", duration: " + duration + ", timeout: " + timeout );
        }
    }
}
