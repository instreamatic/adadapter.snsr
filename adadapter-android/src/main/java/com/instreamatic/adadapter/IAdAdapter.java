package com.instreamatic.adadapter;

public interface IAdAdapter {
    public void preload();

    public void start();

    public void play();

    public void pause();

    public void reset();

    public void skip();

    public boolean isPlaying();

    public boolean isActive();
}
