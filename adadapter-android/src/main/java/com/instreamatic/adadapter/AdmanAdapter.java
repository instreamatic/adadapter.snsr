package com.instreamatic.adadapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.event.EventDispatcher;
import com.instreamatic.adman.module.IAdmanModule;
import com.instreamatic.adman.recognition.IVoiceRecognition;
import com.instreamatic.adman.statistic.LiveStatisticLoader;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.player.IAudioPlayer;
import com.instreamatic.vast.model.VASTInline;

import java.lang.ref.WeakReference;

public class AdmanAdapter {
    final private static String TAG = "AdmanAdapter";

    protected WeakReference<Context> contextRef;

    protected boolean loading;

    //Ad Adman
    private AdmanAdapter.AdmanEventListener admanEventListener = new AdmanAdapter.AdmanEventListener();
    public IAdman adman;

    public AdmanAdapter(Context context, AdmanRequest request) {
        this.contextRef = new WeakReference<>(context);
        this.loading = false;
        initAdman(request);
    }

    public void start() {
        this.load(true);
    }

    public void preload() {
        this.load(false);
    }

    public void load(boolean startPlaying) {
        if (this.isActive()) {
            Log.i(TAG, "Adapter is active");
            return;
        }
        if (adman != null) {
            loading = true;
            if (startPlaying) adman.start();
            else adman.preload();
        }
    }

    public void pause() {
        if (adman != null) {
            adman.pause();
        }
    }

    public void play() {
        if (adman != null) {
            adman.play();
        }
    }

    public void skip() {
        if (adman != null) {
            adman.skip();
        }
    }

    public void reset() {
        if (adman != null) {
            adman.reset();
        }
    }

    public void sendCanShow() {
        if (adman != null) {
            adman.sendCanShow();
        }
    }

    public Bundle getMetadata() {
        if (adman != null) {
            return adman.getMetadata();
        }
        return null;
    }

    public AdmanRequest getRequest() {
        return adman != null ? adman.getRequest(): null;
    }

    public AdmanRequest updateRequest(AdmanRequest.Builder parameters, boolean allField) {
        Log.d(TAG, "Update ad request");
        if (adman != null) {
            adman.updateRequest(parameters, allField);
            return adman.getRequest();
        }
        return null;
    }

    public boolean isPlaying() {
        return adman != null ? adman.isPlaying() : false;
    }

    public boolean isActive() {
        return this.isPlaying() || (this.getCurrentAd() != null);
    }

    public VASTInline getCurrentAd() {
        return adman != null ? adman.getCurrentAd(): null;
    }

    public EventDispatcher getDispatcher() {
        return adman != null ? adman.getDispatcher() : null;
    }

    public IAudioPlayer getМediaPlayer() {
        return adman != null ? adman.getPlayer() : null;
    }

    public void setParameters(Bundle parameters) {
        if (adman != null) {
            adman.setParameters(parameters);
        }
    }

    public void setVASTBytes(byte[] byteArray, boolean startPlaying) {
        if (adman != null) {
            adman.setVASTBytes(byteArray, startPlaying);
        }
    }

    public void bindModule(IAdmanModule module) {
        Log.d(TAG, "bindModule");
        if (adman != null) {
            adman.bindModule(module);
        }
    }

    public void unbindModule(IAdmanModule module) {
        Log.d(TAG, "unbindModule");
        if (adman != null) {
            adman.unbindModule(module);
        }
    }

    public void setVoiceRecognition(IVoiceRecognition voiceRecognition) {
        AdmanVoice admanVoice = adman.getModuleAs(AdmanVoice.ID, AdmanVoice.class);
        if (admanVoice != null) {
            admanVoice.setVoiceRecognition(voiceRecognition);
        }
    }

    public void sendAction(final String action) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (adman != null) {
                    new LiveStatisticLoader().send(adman, action);
                }
            }
        }).start();
    }

    private void initAdman(AdmanRequest request) {
        Context context = contextRef.get();
        if (context == null) {
            Log.d(TAG,"Context is null");
            return;
        }
        //creating Ad object
        adman = new Adman(context, request);
        AdmanVoice admanVoice = new AdmanVoice(context);
        adman.bindModule(admanVoice);

        adman.addListener(admanEventListener);
        //        Bundle  parametersAF = new Bundle();
        //        parametersAF.putBoolean("adman.need_audio_focus", false);
        //        adman.setParameters(parametersAF);
    }

    private class AdmanEventListener implements AdmanEvent.Listener {
        @Override
        public void onAdmanEvent(AdmanEvent event) {
            AdmanEvent.Type type = event.getType();
            Log.d(TAG, "onAdmanEvent: " + type);
            switch (type) {
                case NONE:
                case FAILED:
                case STARTED:
                    loading = false;
                    break;
            }
        }
    }

}
