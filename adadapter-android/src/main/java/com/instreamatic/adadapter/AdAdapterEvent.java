package com.instreamatic.adadapter;

import com.instreamatic.adman.event.BaseEvent;
import com.instreamatic.adman.event.EventListener;
import com.instreamatic.adman.event.EventType;

public class AdAdapterEvent extends BaseEvent<AdAdapterEvent.Type, AdAdapterEvent.Listener> {

    final public static EventType<AdAdapterEvent.Type, AdAdapterEvent, Listener> TYPE = new EventType<AdAdapterEvent.Type, AdAdapterEvent, AdAdapterEvent.Listener>("AdAdapter") {
        @Override
        public void callListener(AdAdapterEvent event, AdAdapterEvent.Listener listener) {
            listener.onAdAdapterEvent(event);
        }
    };

    public enum Type {
        PREPARE,
        NONE,
        FAILED,
        READY,
        STARTED,
        ALMOST_COMPLETE,
        COMPLETED,
        SKIPPED,
        START_DETECT,
        STOP_DETECT,
        START_RECOGNITION,
        STOP_RECOGNITION,
        PAUSE,
        PLAY,
        PROGRESS,
    }
    final public String message;

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender, String message) {
        super(type, sender);
        this.message = message;
    }

    public AdAdapterEvent(AdAdapterEvent.Type type, String sender) {
        this(type, sender, null);
    }

    @Override
    public EventType<AdAdapterEvent.Type, AdAdapterEvent, AdAdapterEvent.Listener> getEventType() {
        return TYPE;
    }

    public interface Listener extends EventListener {
        public void onAdAdapterEvent(AdAdapterEvent event);
    }
}
